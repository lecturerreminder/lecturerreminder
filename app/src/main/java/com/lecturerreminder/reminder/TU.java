package com.lecturerreminder.reminder;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TimePicker;

import java.util.ArrayList;
import java.util.Calendar;

public class TU extends AppCompatActivity implements AdapterView.OnItemClickListener{

    private ActionBarDrawerToggle actionBarDrawerToggle;
    private DrawerLayout drawerLayout;
    private ListView menu;
    private FragmentTransaction fragmentTransaction;
    private FragmentManager fragmentManager;
    private Button rw,rt,kw,sw,st;
    private EditText txtdate,txttime;
    private int mYear, mMonth, mDay, mHour, mMinute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tu);
        setTitle("Tata Usaha");
        drawerLayout = (DrawerLayout) findViewById(R.id.Tdrawer);
        menu = (ListView) findViewById(R.id.TMenu);
        ArrayList<String> menuArray = new ArrayList<String>();
        menuArray.add("List Jadwal");
        menuArray.add("Tambah Jadwal Kelas");
        menuArray.add("Tambah Jadwal Rapat");
        menuArray.add("Tambah Jadwal Seminar");
        menuArray.add("Logout");
        ArrayAdapter<String> Adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_activated_1, menuArray);
        menu.setAdapter(Adapter);
        menu.setOnItemClickListener(this);
        menu.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.opendrawer, R.string.closedrawer);
        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        fragmentManager = getSupportFragmentManager();
        loadSelection(0);

    }
    public void Datepicker(View v)
    {
        rt = (Button) findViewById(R.id.rdate);
        st = (Button) findViewById(R.id.sdate);


        if (v == rt)
        {
            txtdate = (EditText) findViewById(R.id.redate);
        }
        else
        {
            txtdate = (EditText) findViewById(R.id.sedate);
        }

        final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            // Launch Date Picker Dialog
            DatePickerDialog dpd;
            dpd = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            // Display Selected date in textbox
                            txtdate.setText( year+ "-"
                                    + (monthOfYear + 1) + "-" +dayOfMonth );

                        }
                    }, mYear, mMonth, mDay);
            dpd.show();

    }
    public void timepicker(View v) {
        rw = (Button) findViewById(R.id.rtime);
        sw = (Button) findViewById(R.id.stime);
        kw = (Button) findViewById(R.id.ktime);

        if (v == rw) {
            txttime = (EditText) findViewById(R.id.retime);
        } else if (v == sw) {
            txttime = (EditText) findViewById(R.id.setime);
        } else {
            txttime = (EditText) findViewById(R.id.ketime);
        }
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog tpd = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        // Display Selected time in textbox
                        txttime.setText(hourOfDay + ":" + minute);
                    }
                }, mHour, mMinute, false);
        tpd.show();
    }

    private void loadSelection(int i) {
        menu.setItemChecked(i, true);
        if (i == 0) {
            ListJadwalTU A = new ListJadwalTU();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.Tholder,A);
            fragmentTransaction.commit();
        }

        else if (i == 1) {
            TambahJadwalKelas A = new TambahJadwalKelas();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.Tholder,A);
            fragmentTransaction.commit();
        }
        else if (i == 2) {
            TambahJadwalRapat A = new TambahJadwalRapat();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.Tholder,A);
            fragmentTransaction.commit();
        }
        else if (i == 3) {
            TambahJadwalSeminar A = new TambahJadwalSeminar();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.Tholder,A);
            fragmentTransaction.commit();
        }
        else if (i == 4) {
            Intent intent = new Intent(this, Login.class);
            startActivity(intent);
        }


    }
    @Override
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        actionBarDrawerToggle.syncState();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        } else if (id == android.R.id.home) {
            if (drawerLayout.isDrawerOpen(menu)) {
                drawerLayout.closeDrawer(menu);
            } else {
                drawerLayout.openDrawer(menu);
            }
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        loadSelection(position);
        drawerLayout.closeDrawer(menu);
    }
    public void hideKeyboard2(View view)
    {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

}
