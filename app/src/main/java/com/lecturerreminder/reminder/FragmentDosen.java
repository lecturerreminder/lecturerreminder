package com.lecturerreminder.reminder;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by Franco on 1/10/2016.
 */
public class FragmentDosen extends FragmentPagerAdapter {

    public FragmentDosen(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount() {
        return 3;
    }
    @Override
    public Fragment getItem(int position) {
        if (position==0)
            return new kelas_dosen();
        else if (position==1)
            return new rapat_dosen();
        else
            return new seminar_dosen();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position==0)
            return "Jadwal Kelas";
        else if (position==1)
            return "Jadwal Rapat";
        else
            return "Jadwal Seminar";
    }
}
