package com.lecturerreminder.reminder;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 */
public class editM extends Fragment implements View.OnClickListener {
    View myview;
    EditText ID,Nama;
    String id,nama,sks,nnama,nsks;
    Spinner Sks;
    private FragmentTransaction fragmentTransaction;
    private FragmentManager fragmentManager;
    private static final String LOGIN_URL = "http://mobilreminder.pe.hu/edit_matkul.php";
    Button save;
    public editM() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        id = getArguments().getString("id");
        nama =  getArguments().getString("nama");
        sks =  getArguments().getString("sks");
        int a = Integer.parseInt(sks);
        int b = a-1;
        myview = inflater.inflate(R.layout.fragment_edit_m, container, false);
        ID = (EditText) myview.findViewById(R.id.editText13);
        Nama = (EditText) myview.findViewById(R.id.editText14);
        Sks = (Spinner) myview.findViewById(R.id.spinner1);
        save = (Button) myview.findViewById(R.id.button8);
        save.setOnClickListener(this);
        fragmentManager = getActivity().getSupportFragmentManager();
        ID.setText(id);
        Nama.setText(nama);
        Sks.setSelection(b);
        ID.setEnabled(false);

        return myview;
    }


    @Override
    public void onClick(View v) {

        nnama = Nama.getText().toString();
        nsks = Sks.getSelectedItem().toString();
        if(nnama.isEmpty()||nsks.isEmpty())
        {
            Toast.makeText(getActivity(), "Please Fill all the Field", Toast.LENGTH_LONG).show();
        }
        else {

            bgpc(id, nnama, nsks);
        }
    }
    public void bgpc(final String id, String nama, String sks)
    {
        class Asyncpc extends AsyncTask<String, Void, String>
        {
            private Dialog loading;

            @Override
            protected void onPreExecute()
            {
                super.onPreExecute();
                loading = ProgressDialog.show(getActivity(), "Please wait", "Loading...");
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(getActivity(), s, Toast.LENGTH_LONG).show();
                EditMatkul A = new EditMatkul();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.Aholder, A);
                fragmentTransaction.commit();
            }

            @Override
            protected String doInBackground(String... params) {

                HashMap<String, String> data = new HashMap<>();
                data.put("id", params[0]);
                data.put("nama", params[1]);
                data.put("sks", params[2]);

                Request ruc  = new Request();
                String result = ruc.Con(LOGIN_URL, data);

                return result;
            }

        }
        Asyncpc ulc = new Asyncpc();
        ulc.execute(id,nama,sks);
    }
}
