package com.lecturerreminder.reminder;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class TambahJadwalSeminar extends Fragment implements View.OnClickListener{
    private AutoCompleteTextView Dopem1,Dopem2,Dpm1,Dpm2,TA;
    Button save ;
    private View myview;
    EditText edate,etime,ehal;
    String date,time,hal,dp1,dp2,d1,d2,kor;
    private ArrayAdapter<String> adapter;
    String myJSON;
    int x=0;
    JSONArray dosen = null;
    String[] ld = new String[100];
    private static final String TAG_RESULTS = "result";
    private static final String TAG_NAME = "name";
    private static final String JSON_URL = "http://mobilreminder.pe.hu/editdosen.php";
    private static final String LOGIN_URL = "http://mobilreminder.pe.hu/tambah_seminar.php";

    public TambahJadwalSeminar() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myview = inflater.inflate(R.layout.fragment_tambah_jadwal_seminar, container, false);
        getData(JSON_URL);
        Dopem1 = (AutoCompleteTextView) myview.findViewById(R.id.Dpem1);
        Dopem2 = (AutoCompleteTextView) myview.findViewById(R.id.Dpem2);
        Dpm1 = (AutoCompleteTextView) myview.findViewById(R.id.Dpm1);
        Dpm2 = (AutoCompleteTextView) myview.findViewById(R.id.Dpm2);
        TA = (AutoCompleteTextView) myview.findViewById(R.id.TA);
        edate = (EditText) myview.findViewById(R.id.sedate);
        etime = (EditText) myview.findViewById(R.id.setime);
        ehal = (EditText) myview.findViewById(R.id.editText6);
        save = (Button) myview.findViewById(R.id.button2);
        save.setOnClickListener(this);
        return myview;
    }

    public void onClick(View v) {
        d1 = Dopem1.getText().toString();
        d2 = Dopem2.getText().toString();
        dp1 = Dpm1.getText().toString();
        dp2 = Dpm2.getText().toString();
        kor = TA.getText().toString();
        date = edate.getText().toString();
        time = etime.getText().toString();
        hal = ehal.getText().toString();
        if (d1.isEmpty() || d2.isEmpty() || dp1.isEmpty() || dp2.isEmpty() || kor.isEmpty() || date.isEmpty() || time.isEmpty() || hal.isEmpty()) {
            Toast.makeText(getActivity(), "Please Fill all the Field", Toast.LENGTH_LONG).show();
        } else {
            bgpc(d1,d2,dp1,dp2,kor,date,time,hal);
        }
    }
    public void bgpc(final String d1, String d2, String dp1, String dp2,String kor, String date, String time, String hal)
    {
        class Asyncpc extends AsyncTask<String, Void, String>
        {
            private Dialog loading;

            @Override
            protected void onPreExecute()
            {
                super.onPreExecute();
                loading = ProgressDialog.show(getActivity(), "Please wait", "Loading...");
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(getActivity(), s, Toast.LENGTH_LONG).show();
                Dopem2.setText("");
                Dopem1.setText("");
                Dpm1.setText("");
                Dpm2.setText("");
                TA.setText("");
                etime.setText("");
                edate.setText("");
                ehal.setText("");
            }

            @Override
            protected String doInBackground(String... params) {

                HashMap<String, String> data = new HashMap<>();
                data.put("dop1", params[0]);
                data.put("dop2", params[1]);
                data.put("dopm1", params[2]);
                data.put("dopm2",params[3]);
                data.put("kor",params[4]);
                data.put("tgl",params[5]);
                data.put("jam",params[6]);
                data.put("perihal",params[7]);

                Request ruc  = new Request();
                String result = ruc.Con(LOGIN_URL, data);

                return result;
            }

        }
        Asyncpc ulc = new Asyncpc();
        ulc.execute(d1,d2,dp1,dp2,kor,date,time,hal);
    }

    public void getData(String url) {
        class GetDataJSON extends AsyncTask<String, Void, String> {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... params) {
                String uri = params[0];
                String result;
                Request ruc = new Request();
                result = ruc.get(uri);
                return result;
            }

            @Override
            protected void onPostExecute(String result) {
                myJSON = result;
                showList();

            }
        }
        GetDataJSON g = new GetDataJSON();
        g.execute(url);
    }

    private void showList() {
        try {
            JSONObject jsonObj = new JSONObject(myJSON);
            dosen = jsonObj.getJSONArray(TAG_RESULTS);
            for(int i=0;i<dosen.length();i++){
                JSONObject c = dosen.getJSONObject(i);
                ld[i] = c.getString(TAG_NAME);
            }
        }catch (JSONException e) {
            e.printStackTrace();
        }
        List<String> dosen = new ArrayList<String>();

        while(x<100)
        {
            if(ld[x]==null)
            {}
            else
            {
                dosen.add(ld[x]);
            }

            x++;
        }

        adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, dosen);
        Dopem2.setAdapter(adapter);
        Dopem1.setAdapter(adapter);
        Dpm1.setAdapter(adapter);
        Dpm2.setAdapter(adapter);
        TA.setAdapter(adapter);

    }
}
