package com.lecturerreminder.reminder;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

public class Admin extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private String url;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private DrawerLayout drawerLayout;
    private ListView menu;
    private FragmentTransaction fragmentTransaction;
    private FragmentManager fragmentManager;
    private Button du, dd, dm, eu, ed, em;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        setTitle("Admin");
        drawerLayout = (DrawerLayout) findViewById(R.id.Adrawer);
        ArrayList<String> menuArray = new ArrayList<String>();
        menuArray.add("Tambah Mata Kuliah");
        menuArray.add("Tambah TU");
        menuArray.add("Tambah Dosen");
        menuArray.add("Edit Mata Kuliah");
        menuArray.add("Edit User");
        menuArray.add("Edit Dosen");
        menuArray.add("Logout");
        ArrayAdapter<String> Adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_activated_1, menuArray);
        menu = (ListView) findViewById(R.id.AMenu);
        menu.setAdapter(Adapter);
        menu.setOnItemClickListener(this);
        menu.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.opendrawer, R.string.closedrawer);
        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        fragmentManager = getSupportFragmentManager();

        loadSelection(0);
    }

    private void loadSelection(int i) {
        menu.setItemChecked(i, true);

        if (i == 0) {
            TambahMatkul A = new TambahMatkul();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.Aholder, A);
            fragmentTransaction.commit();
        } else if (i == 1) {
            TambahUser A = new TambahUser();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.Aholder, A);
            fragmentTransaction.commit();
        } else if (i == 2) {
            TambahDosen A = new TambahDosen();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.Aholder, A);
            fragmentTransaction.commit();
        } else if (i == 3) {
            EditMatkul A = new EditMatkul();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.Aholder, A);
            fragmentTransaction.commit();
        } else if (i == 4) {
            EditUser A = new EditUser();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.Aholder, A);
            fragmentTransaction.commit();
        } else if (i == 5) {
            EditDosen A = new EditDosen();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.Aholder, A);
            fragmentTransaction.commit();
        } else if (i == 6) {
            Intent intent = new Intent(this, Login.class);
            startActivity(intent);
        }

    }

    @Override
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        actionBarDrawerToggle.syncState();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        } else if (id == android.R.id.home) {
            if (drawerLayout.isDrawerOpen(menu)) {
                drawerLayout.closeDrawer(menu);
            } else {
                drawerLayout.openDrawer(menu);
            }
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        loadSelection(position);
        drawerLayout.closeDrawer(menu);
    }

    public void hideKeyboard(View view) {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    public void Delete(View v) {

        du = (Button) findViewById(R.id.deleteU);
        dd = (Button) findViewById(R.id.deleteD);
        dm = (Button) findViewById(R.id.deleteM);
        if (v == du) {
            url = "http://mobilreminder.pe.hu/deltu.php";
        } else if (v == dd) {
            url = "http://mobilreminder.pe.hu/deldosen.php";
        } else {
            url = "http://mobilreminder.pe.hu/delmatkul.php";
        }

        final TextView ID;
        final String id;
        ID = (TextView) ((View) v.getParent()).findViewById(R.id.id);
        id = ID.getText().toString();
        final AlertDialog alertDialog = new AlertDialog.Builder(Admin.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("Are you sure you want to Delete?");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        bgdel(id, url);
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public void Edit(View v) {
        String id,nama,sks;
        TextView ID,Nama,Sks;
        ID = (TextView) ((View) v.getParent()).findViewById(R.id.id);
        id = ID.getText().toString();
        Nama = (TextView) ((View) v.getParent()).findViewById(R.id.name);
        nama = Nama.getText().toString();
        eu = (Button) findViewById(R.id.editU);
        ed = (Button) findViewById(R.id.editD);
        em = (Button) findViewById(R.id.editM);
        switch(v.getId())
        {
            case R.id.editU:
                Bundle bundle=new Bundle();
                bundle.putString("id", id);
                bundle.putString("nama",nama);
                editU A = new editU();
                A.setArguments(bundle);
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.Aholder, A);
                fragmentTransaction.commit();
                break;
            case R.id.editD:
                Bundle bundle2=new Bundle();
                bundle2.putString("id", id);
                bundle2.putString("nama",nama);
                editD B = new editD();
                B.setArguments(bundle2);
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.Aholder, B);
                fragmentTransaction.commit();
                break;
            case R.id.editM:
                Sks = (TextView) ((View) v.getParent()).findViewById(R.id.sks);
                sks = Sks.getText().toString();
                Bundle bundle3=new Bundle();
                bundle3.putString("id", id);
                bundle3.putString("nama",nama);
                bundle3.putString("sks",sks);
                editM C = new editM();
                C.setArguments(bundle3);
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.Aholder, C);
                fragmentTransaction.commit();
                break;
        }


    }

    public void bgdel(final String id, final String url) {
        class Asyncpc extends AsyncTask<String, Void, String> {
            private Dialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(Admin.this, "Please wait", "Loading...");
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(Admin.this, s, Toast.LENGTH_LONG).show();
            }

            @Override
            protected String doInBackground(String... params) {

                HashMap<String, String> data = new HashMap<>();
                data.put("id", params[0]);
                Request ruc = new Request();
                String result = ruc.Con(url, data);

                return result;
            }

        }
        Asyncpc ulc = new Asyncpc();
        ulc.execute(id, url);
    }

}