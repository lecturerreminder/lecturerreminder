package com.lecturerreminder.reminder;


import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 */
public class rapat_dosen extends Fragment {

    String id;
    String myJSON;
    View myview;
    ListView List;
    private static final String TAG_RESULTS="result";
    private static final String TAG_ID="id";
    private static final String TAG_HAL="hal";
    private static final String TAG_TGL="tanggal";
    private static final String TAG_JAM="jam";
    private static final String TAG_LOKASI="lokasi";
    private static final String JSON_URL = "http://mobilreminder.pe.hu/listrapat.php";
    JSONArray peoples = null;
    ArrayList<HashMap<String,String>> personList;

    public rapat_dosen() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        myview = inflater.inflate(R.layout.fragment_rapat_dosen, container, false);
        List = (ListView) myview.findViewById(R.id.listView);
        personList = new ArrayList<HashMap<String,String>>();
        getData(JSON_URL);
        return myview;
    }

    protected void showList(){
        try {
            JSONObject jsonObj = new JSONObject(myJSON);
            peoples = jsonObj.getJSONArray(TAG_RESULTS);

            for(int i=0;i<peoples.length();i++){
                JSONObject c = peoples.getJSONObject(i);
                String hal = c.getString(TAG_HAL);
                String tanggal = c.getString(TAG_TGL);
                String jam = c.getString(TAG_JAM);
                String lokasi = c.getString(TAG_LOKASI);

                HashMap<String,String> persons = new HashMap<String,String>();

                persons.put(TAG_HAL,hal);
                persons.put(TAG_TGL,tanggal);
                persons.put(TAG_JAM,jam);
                persons.put(TAG_LOKASI,lokasi);
                personList.add(persons);
            }



            ListAdapter adapter = new SimpleAdapter(
                    getActivity(), personList, R.layout.rapat_dosen,
                    new String[]{TAG_HAL,TAG_TGL,TAG_JAM,TAG_LOKASI},
                    new int[]{R.id.hal,R.id.tgl,R.id.jam,R.id.lks}
            );

            List.setAdapter(adapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void getData(String url){
        class GetDataJSON extends AsyncTask<String, Void, String> {
            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(getActivity(),"Please Wait...",null,true,true);
            }

            @Override
            protected String doInBackground(String... params) {
                String uri=params[0];
                String result;
                Request ruc = new Request();
                result = ruc.get(uri);
                return result;
            }

            @Override
            protected void onPostExecute(String result){
                myJSON=result;
                showList();
                loading.dismiss();
            }
        }
        GetDataJSON g = new GetDataJSON();
        g.execute(url);
    }

}
