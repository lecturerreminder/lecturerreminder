package com.lecturerreminder.reminder;


import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 */
public class EditUser extends Fragment {

    String myJSON;
    private static final String TAG_RESULTS="result";
    private static final String TAG_ID="id";
    private static final String TAG_NAME="name";
    private static final String JSON_URL = "http://mobilreminder.pe.hu/edittu.php";
    JSONArray peoples = null;
    ArrayList<HashMap<String,String>> personList;
    ListView List;
    View myview;

    public EditUser() {

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        myview = inflater.inflate(R.layout.fragment_edit_user, container, false);
        List = (ListView) myview.findViewById(R.id.listView);
        personList = new ArrayList<HashMap<String,String>>();
        getData(JSON_URL);
        return myview;
    }



    protected void showList(){
        try {
            JSONObject jsonObj = new JSONObject(myJSON);
            peoples = jsonObj.getJSONArray(TAG_RESULTS);

            for(int i=0;i<peoples.length();i++){
                JSONObject c = peoples.getJSONObject(i);
                String id = c.getString(TAG_ID);
                String name = c.getString(TAG_NAME);

                HashMap<String,String> persons = new HashMap<String,String>();

                persons.put(TAG_ID,id);
                persons.put(TAG_NAME,name);
                personList.add(persons);
            }



            ListAdapter adapter = new SimpleAdapter(
                    getActivity(), personList, R.layout.list_item,
                    new String[]{TAG_ID,TAG_NAME},
                    new int[]{R.id.id, R.id.name}
            );

            List.setAdapter(adapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void getData(String url){
        class GetDataJSON extends AsyncTask<String, Void, String> {
            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(getActivity(),"Please Wait...",null,true,true);
            }

            @Override
            protected String doInBackground(String... params) {
                String uri=params[0];
                String result;
                Request ruc = new Request();
                result = ruc.get(uri);
                return result;
            }

            @Override
            protected void onPostExecute(String result){
                myJSON=result;
                showList();
                loading.dismiss();
            }
        }
        GetDataJSON g = new GetDataJSON();
        g.execute(url);
    }

}
