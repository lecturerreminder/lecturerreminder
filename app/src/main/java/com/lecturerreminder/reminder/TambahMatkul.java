package com.lecturerreminder.reminder;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.net.URL;
import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 */
public class TambahMatkul extends Fragment implements View.OnClickListener {

    Button save;
    EditText eid,enama;
    Spinner esks;
    String id,nama,sks;
    View myview;
    private static final String LOGIN_URL = "http://mobilreminder.pe.hu/tambahmatkul.php";
    public TambahMatkul() {
        // Required empty public constructor
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        myview = inflater.inflate(R.layout.fragment_tambah_matkul, container, false);
        save = (Button) myview.findViewById(R.id.button8);
        save.setOnClickListener(this);
        eid = (EditText) myview.findViewById(R.id.editText13);
        enama = (EditText) myview.findViewById(R.id.editText14);
        esks = (Spinner) myview.findViewById(R.id.spinner1);
        return myview;
    }

    @Override
    public void onClick(View v) {

        id = eid.getText().toString();
        nama = enama.getText().toString();
        sks = esks.getSelectedItem().toString();
        if(id.isEmpty()||nama.isEmpty()||sks.isEmpty())
        {
            Toast.makeText(getActivity(), "Please Fill all the Field", Toast.LENGTH_LONG).show();
        }
        else {

            bgpc(id, nama, sks);
        }
    }

    public void bgpc(final String id, String nama, String sks)
    {
        class Asyncpc extends AsyncTask<String, Void, String>
        {
            private Dialog loading;

            @Override
            protected void onPreExecute()
            {
                super.onPreExecute();
                loading = ProgressDialog.show(getActivity(),"Please wait", "Loading...");
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(getActivity(), s, Toast.LENGTH_LONG).show();
                eid.setText("");
                enama.setText("");
                esks.setSelection(0);
            }

            @Override
            protected String doInBackground(String... params) {

                HashMap<String, String> data = new HashMap<>();
                data.put("id", params[0]);
                data.put("nama", params[1]);
                data.put("sks", params[2]);

                Request ruc  = new Request();
                String result = ruc.Con(LOGIN_URL, data);

                return result;
            }

        }
        Asyncpc ulc = new Asyncpc();
        ulc.execute(id,nama,sks);
    }
}
