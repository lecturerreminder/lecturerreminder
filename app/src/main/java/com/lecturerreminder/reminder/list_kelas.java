package com.lecturerreminder.reminder;


import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 */
public class list_kelas extends Fragment {

    String myJSON;
    private static final String TAG_RESULTS="result";
    private static final String TAG_ID="id";
    private static final String TAG_MTK="matakuliah";
    private static final String TAG_DSN="dosen";
    private static final String TAG_hari="hari";
    private static final String TAG_jam="jam";
    private FragmentTransaction fragmentTransaction;
    private FragmentManager fragmentManager;
    private static final String JSON_URL = "http://mobilreminder.pe.hu/listkelas.php";
    JSONArray peoples = null;
    ArrayList<HashMap<String,String>> personList;
    ListView List;
    View myview,aa;

    public list_kelas() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        myview =inflater.inflate(R.layout.fragment_list_kelas, container, false);
        List = (ListView) myview.findViewById(R.id.listView);
        fragmentManager = getActivity().getSupportFragmentManager();
        personList = new ArrayList<HashMap<String,String>>();
        getData(JSON_URL);
       List.setOnItemClickListener(new AdapterView.OnItemClickListener() {
           @Override
           public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               final String aha,url,dsn,mtk,hr,jam;
               final TextView a,edsn,emtk,ehr,ejam;
               final Button delete,edit;
               delete = (Button) view.findViewById(R.id.delete);
               edit = (Button) view.findViewById(R.id.edit);
               a = (TextView) view.findViewById(R.id.id);
               edsn = (TextView) view.findViewById(R.id.Dosen);
               emtk = (TextView) view.findViewById(R.id.mtk);
               ehr = (TextView) view.findViewById(R.id.hari);
               ejam = (TextView) view.findViewById(R.id.jam);
               aha = a.getText().toString();
               dsn = edsn.getText().toString();
               mtk = emtk.getText().toString();
               hr = ehr.getText().toString();
               jam = ejam.getText().toString();
               url = "http://mobilreminder.pe.hu/delkelas.php";


               delete.setOnClickListener(new View.OnClickListener() {
                   @Override
                   public void onClick(View v) {
                       final AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                       alertDialog.setTitle("Alert");
                       alertDialog.setMessage("Are you sure you want to Delete?");
                       alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                               new DialogInterface.OnClickListener() {
                                   public void onClick(DialogInterface dialog, int which) {
                                       bgdel(aha, url);
                                   }
                               });
                       alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel",
                               new DialogInterface.OnClickListener() {
                                   public void onClick(DialogInterface dialog, int which) {
                                       dialog.dismiss();
                                   }
                               });
                       alertDialog.show();
                   }
               });

               edit.setOnClickListener(new View.OnClickListener() {
                   @Override
                   public void onClick(View v)
                   {
                       Bundle bundle=new Bundle();
                       bundle.putString("id", aha);
                       bundle.putString("dsn",dsn);
                       bundle.putString("mtk",mtk);
                       bundle.putString("hr",hr);
                       bundle.putString("jam",jam);
                       Editkelas A = new Editkelas();
                       A.setArguments(bundle);
                       fragmentTransaction = fragmentManager.beginTransaction();
                       fragmentTransaction.replace(R.id.Tholder, A);
                       fragmentTransaction.commit();
                   }
               });
           }
       });
        return myview;
    }
    public void bgdel(final String id, final String url) {
        class Asyncpc extends AsyncTask<String, Void, String> {
            private Dialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(getActivity(), "Please wait", "Loading...");
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(getActivity(), s, Toast.LENGTH_LONG).show();
            }

            @Override
            protected String doInBackground(String... params) {

                HashMap<String, String> data = new HashMap<>();
                data.put("id", params[0]);
                Request ruc = new Request();
                String result = ruc.Con(url, data);

                return result;
            }

        }
        Asyncpc ulc = new Asyncpc();
        ulc.execute(id, url);
    }

    protected void showList(){
        try {
            JSONObject jsonObj = new JSONObject(myJSON);
            peoples = jsonObj.getJSONArray(TAG_RESULTS);

            for(int i=0;i<peoples.length();i++){
                JSONObject c = peoples.getJSONObject(i);
                String id = c.getString(TAG_ID);
                String mtk = c.getString(TAG_MTK);
                String dsn = c.getString(TAG_DSN);
                String hari = c.getString(TAG_hari);
                String jam = c.getString(TAG_jam);

                HashMap<String,String> persons = new HashMap<String,String>();

                persons.put(TAG_ID,id);
                persons.put(TAG_MTK,mtk);
                persons.put(TAG_DSN,dsn);
                persons.put(TAG_hari,hari);
                persons.put(TAG_jam,jam);
                personList.add(persons);
            }



            ListAdapter adapter = new SimpleAdapter(
                    getActivity(), personList, R.layout.item_kelas,
                    new String[]{TAG_ID,TAG_MTK,TAG_DSN,TAG_hari,TAG_jam},
                    new int[]{R.id.id, R.id.mtk,R.id.Dosen,R.id.hari,R.id.jam}
            );

            List.setAdapter(adapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void getData(String url){
        class GetDataJSON extends AsyncTask<String, Void, String> {
            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(getActivity(),"Please Wait...",null,true,true);
            }

            @Override
            protected String doInBackground(String... params) {
                String uri=params[0];
                String result;
                Request ruc = new Request();
                result = ruc.get(uri);
                return result;
            }

            @Override
            protected void onPostExecute(String result){
                myJSON=result;
                showList();
                loading.dismiss();
            }
        }
        GetDataJSON g = new GetDataJSON();
        g.execute(url);
    }

}
