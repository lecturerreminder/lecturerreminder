package com.lecturerreminder.reminder;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class TambahJadwalKelas extends Fragment implements View.OnClickListener{
    private AutoCompleteTextView Dosen,Matkul;
    private View myview;
    private ArrayAdapter<String> adapter,adapter2;
    String dsn,mtk,hr,tm;
    EditText etime;
    Button save;
    String myJSON;
    String myJSON2;
    int x=0,x2=0;
    JSONArray dosen = null;
    JSONArray matkul = null;
    String[] ld = new String[100];
    String[] mt = new String[100];
    Spinner hari;
    private static final String TAG_RESULTS = "result";
    private static final String TAG_NAME = "name";
    private static final String JSON_URL = "http://mobilreminder.pe.hu/editdosen.php";
    private static final String JSON_URL2 = "http://mobilreminder.pe.hu/editmatkul.php";
    private static final String LOGIN_URL = "http://mobilreminder.pe.hu/tambah_kelas.php";


    public TambahJadwalKelas() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myview = inflater.inflate(R.layout.fragment_tambah_jadwal_kelas, container, false);
        getData(JSON_URL);
        getData2(JSON_URL2);
        save = (Button) myview.findViewById(R.id.button4);
        save.setOnClickListener(this);
        hari = (Spinner) myview.findViewById(R.id.spinner);
        etime = (EditText) myview.findViewById(R.id.ketime);
        Dosen = (AutoCompleteTextView) myview.findViewById(R.id.Dosen);
        Matkul = (AutoCompleteTextView) myview.findViewById(R.id.Matkul);
        return myview;
    }
    @Override
    public void onClick(View v) {
        dsn = Dosen.getText().toString();
        mtk = Matkul.getText().toString();
        hr = hari.getSelectedItem().toString();
        tm = etime.getText().toString();
        if (dsn.isEmpty() || mtk.isEmpty() || hr.isEmpty() || tm.isEmpty()) {
            Toast.makeText(getActivity(), "Please Fill all the Field", Toast.LENGTH_LONG).show();
        } else {
            bgpc(dsn, mtk, hr, tm);
        }
    }
    public void bgpc(final String dosen, String mata, String hari, String waktu)
    {
        class Asyncpc extends AsyncTask<String, Void, String>
        {
            private Dialog loading;

            @Override
            protected void onPreExecute()
            {
                super.onPreExecute();
                loading = ProgressDialog.show(getActivity(), "Please wait", "Loading...");
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(getActivity(), s, Toast.LENGTH_LONG).show();
                Dosen.setText("");
                etime.setText("");
                Matkul.setText("");
            }

            @Override
            protected String doInBackground(String... params) {

                HashMap<String, String> data = new HashMap<>();
                data.put("dop1", params[0]);
                data.put("nama_matkul", params[1]);
                data.put("hari", params[2]);
                data.put("jam",params[3]);

                Request ruc  = new Request();
                String result = ruc.Con(LOGIN_URL, data);

                return result;
            }

        }
        Asyncpc ulc = new Asyncpc();
        ulc.execute(dosen, mata, hari, waktu);
    }
    public void getData(String url) {
        class GetDataJSON extends AsyncTask<String, Void, String> {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... params) {
                String uri = params[0];
                String result;
                Request ruc = new Request();
                result = ruc.get(uri);
                return result;
            }

            @Override
            protected void onPostExecute(String result) {
                myJSON = result;
                showList();

            }
        }
        GetDataJSON g = new GetDataJSON();
        g.execute(url);
    }

    private void showList() {
        try {
            JSONObject jsonObj = new JSONObject(myJSON);
            dosen = jsonObj.getJSONArray(TAG_RESULTS);
            for(int i=0;i<dosen.length();i++){
                JSONObject c = dosen.getJSONObject(i);
                ld[i] = c.getString(TAG_NAME);
            }
        }catch (JSONException e) {
            e.printStackTrace();
        }
        List<String> dos = new ArrayList<String>();
        while(x<100)
        {
            if(ld[x]==null)
            {}
            else
            {
                dos.add(ld[x]);
            }
            x++;
        }

        adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, dos);
        Dosen.setAdapter(adapter);
    }


    public void getData2(String url) {
        class GetDataJSON extends AsyncTask<String, Void, String> {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... params) {
                String uri = params[0];
                String result;
                Request ruc = new Request();
                result = ruc.get(uri);
                return result;
            }

            @Override
            protected void onPostExecute(String result) {
                myJSON2 = result;
                showList2();

            }
        }
        GetDataJSON g = new GetDataJSON();
        g.execute(url);
    }

    private void showList2() {
        try {
            JSONObject jsonObj = new JSONObject(myJSON2);
            matkul = jsonObj.getJSONArray(TAG_RESULTS);
            for(int i=0;i<matkul.length();i++){
                JSONObject c = matkul.getJSONObject(i);
                mt[i] = c.getString(TAG_NAME);
            }
        }catch (JSONException e) {
            e.printStackTrace();
        }
        List<String> mat = new ArrayList<String>();
        while(x2<100)
        {
            if(mt[x2]==null)
            {}
            else
            {
                mat.add(mt[x2]);
            }

            x2++;
        }

        adapter2 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, mat);
        Matkul.setAdapter(adapter2);
    }


}
