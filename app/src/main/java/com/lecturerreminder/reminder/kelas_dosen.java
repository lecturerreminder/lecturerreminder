package com.lecturerreminder.reminder;


import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 */
public class kelas_dosen extends Fragment {

    String myJSON;
    private static final String TAG_RESULTS="result";
    private static final String TAG_MTK="matakuliah";
    private static final String TAG_hari="hari";
    private static final String TAG_jam="jam";
    private static final String JSON_URL = "http://mobilreminder.pe.hu/listkelasd.php";
    JSONArray peoples = null;
    ArrayList<HashMap<String,String>> personList;
    ListView List;
    String id;
    View myview;
    public kelas_dosen() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        myview = inflater.inflate(R.layout.fragment_kelas_dosen, container, false);
        Bundle bundle = getActivity().getIntent().getExtras();
        id = bundle.getString("id");
        List = (ListView) myview.findViewById(R.id.listView);
        personList = new ArrayList<HashMap<String,String>>();
        getData(JSON_URL,id);
        return myview;
    }
    protected void showList(){
        try {
            JSONObject jsonObj = new JSONObject(myJSON);
            peoples = jsonObj.getJSONArray(TAG_RESULTS);

            for(int i=0;i<peoples.length();i++){
                JSONObject c = peoples.getJSONObject(i);
                String mtk = c.getString(TAG_MTK);
                String hari = c.getString(TAG_hari);
                String jam = c.getString(TAG_jam);

                HashMap<String,String> persons = new HashMap<String,String>();

                persons.put(TAG_MTK,mtk);
                persons.put(TAG_hari,hari);
                persons.put(TAG_jam,jam);
                personList.add(persons);
            }



            ListAdapter adapter = new SimpleAdapter(
                    getActivity(), personList, R.layout.kelas_dosen,
                    new String[]{TAG_MTK,TAG_hari,TAG_jam},
                    new int[]{ R.id.mtk,R.id.hari,R.id.jam}
            );

            List.setAdapter(adapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void getData(String url, String id ){
        class GetDataJSON extends AsyncTask<String, Void, String> {
            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(getActivity(),"Please Wait...",null,true,true);
            }

            @Override
            protected String doInBackground(String... params) {
                String uri=params[0];
                HashMap<String, String> data = new HashMap<>();
                data.put("id", params[1]);
                String result;
                Request ruc = new Request();
                result = ruc.getmore(uri,data);
                return result;
            }

            @Override
            protected void onPostExecute(String result){
                myJSON=result;
                showList();
                loading.dismiss();
            }
        }
        GetDataJSON g = new GetDataJSON();
        g.execute(url,id);
    }


}
