package com.lecturerreminder.reminder;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v4.app.DialogFragment;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 */
public class TambahJadwalRapat extends Fragment implements View.OnClickListener{

    Button save ;
    EditText edate,etime,elokasi,ehal;
    String date,time,lokasi,hal;
    View myview;
    private static final String LOGIN_URL = "http://mobilreminder.pe.hu/tambahrapat.php";

    public TambahJadwalRapat() {
        // Required empty public constructor
    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        myview = inflater.inflate(R.layout.fragment_tambah_jadwal_rapat, container, false);
        save = (Button) myview.findViewById(R.id.button6);
        save.setOnClickListener(this);
        edate = (EditText) myview.findViewById(R.id.redate);
        etime = (EditText) myview.findViewById(R.id.retime);
        elokasi = (EditText) myview.findViewById(R.id.editText11);
        ehal = (EditText) myview.findViewById(R.id.editText12);
        return myview ;
    }

    @Override
    public void onClick(View v) {
        date = edate.getText().toString();
        time = etime.getText().toString();
        lokasi = elokasi.getText().toString();
        hal = ehal.getText().toString();
        if(date.isEmpty()||time.isEmpty()||lokasi.isEmpty()||hal.isEmpty())
        {
            Toast.makeText(getActivity(), "Please Fill all the Field", Toast.LENGTH_LONG).show();
        }
        else {
            bgpc(date, time, lokasi, hal);
        }
    }
    public void bgpc(final String date, String time, String lokasi, String hal)
    {
        class Asyncpc extends AsyncTask<String, Void, String>
        {
            private Dialog loading;

            @Override
            protected void onPreExecute()
            {
                super.onPreExecute();
                loading = ProgressDialog.show(getActivity(), "Please wait", "Loading...");
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(getActivity(), s, Toast.LENGTH_LONG).show();
                edate.setText("");
                etime.setText("");
                elokasi.setText("");
                ehal.setText("");
            }

            @Override
            protected String doInBackground(String... params) {

                HashMap<String, String> data = new HashMap<>();
                data.put("tgl", params[0]);
                data.put("jam", params[1]);
                data.put("lokasi", params[2]);
                data.put("hal",params[3]);

                Request ruc  = new Request();
                String result = ruc.Con(LOGIN_URL, data);

                return result;
            }

        }
        Asyncpc ulc = new Asyncpc();
        ulc.execute(date, time, lokasi, hal);
    }
}
