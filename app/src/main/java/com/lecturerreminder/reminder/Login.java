package com.lecturerreminder.reminder;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import java.net.URL;
import java.util.HashMap;

public class Login extends Activity {

    private EditText etusername;
    private EditText etpassword;
    String username;
    String password;
    private static final String LOGIN_URL = "http://mobilreminder.pe.hu/Login.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        etusername = (EditText) findViewById(R.id.username);
        etpassword = (EditText) findViewById(R.id.password);
    }
    //startActivity(new Intent(Login.this, Admin.class));
    public void dologin(View v)
    {
        username = etusername.getText().toString();
        password = etpassword.getText().toString();
        bglogin(username,password);
    }
    public void bglogin(final String user, String pass)
    {
        class LoginAsync extends AsyncTask<String, Void, String>
        {
            private Dialog loading;

            @Override
            protected void onPreExecute()
            {
                super.onPreExecute();
                loading = ProgressDialog.show(Login.this,"Please wait","Loading...");
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                String type = s.split(",")[0];
                String id = s.split(",")[1];
                if(type.equalsIgnoreCase("Admin"))
                {
                    Intent intent = new Intent(Login.this,Admin.class);
                    startActivity(intent);
                }
                else if(type.equalsIgnoreCase("Dosen"))
                {
                    Intent intent = new Intent(Login.this,Dosen.class);
                    intent.putExtra("id",id);
                    startActivity(intent);
                }
                else if(type.equalsIgnoreCase("TU"))
                {
                    Intent intent = new Intent(Login.this,TU.class);
                    startActivity(intent);
                }
                else
                {
                    Toast.makeText(Login.this,s,Toast.LENGTH_LONG).show();
                }
            }

            @Override
            protected String doInBackground(String... params) {
                URL url;
                String response = "";
                HashMap<String, String> data = new HashMap<>();
                data.put("username", params[0]);
                data.put("password", params[1]);

                Request ruc  = new Request();
                String result = ruc.Con(LOGIN_URL, data);

                return result;
            }

        }
        LoginAsync ulc = new LoginAsync();
        ulc.execute(username,password);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }
}
