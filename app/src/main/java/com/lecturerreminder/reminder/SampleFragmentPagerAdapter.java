package com.lecturerreminder.reminder;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by Franco on 1/1/2016.
 */
public class SampleFragmentPagerAdapter extends FragmentPagerAdapter {
    final int PAGE_COUNT = 3;

    private String tabTitles[] = new String[] { "A", "B", "C" };
    private Context context;

    public SampleFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount() {
        return 3;
    }
    @Override
    public Fragment getItem(int position) {
        if (position==0)
            return new list_kelas();
        else if (position==1)
            return new list_rapat();
        else
            return new list_seminar();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position==0)
            return "Jadwal Kelas";
        else if (position==1)
            return "Jadwal Rapat";
        else
            return "Jadwal Seminar";
    }
}
