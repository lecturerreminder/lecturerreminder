package com.lecturerreminder.reminder;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 */
public class editrapat extends Fragment implements View.OnClickListener{

    EditText  etgl,ejam,elks,ehal;
    String id,tgl,jam,lks,hal;
    View myview;
    Button save;
    private FragmentTransaction fragmentTransaction;
    private FragmentManager fragmentManager;
    private static final String LOGIN_URL = "http://mobilreminder.pe.hu/edit_rapat.php";

    public editrapat() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        myview = inflater.inflate(R.layout.fragment_editrapat, container, false);
        id = getArguments().getString("id");
        tgl = getArguments().getString("tgl");
        jam = getArguments().getString("jam");
        hal = getArguments().getString("hal");
        lks = getArguments().getString("lks");
        etgl = (EditText) myview.findViewById(R.id.redate);
        ejam = (EditText) myview.findViewById(R.id.retime);
        elks = (EditText) myview.findViewById(R.id.editText11);
        ehal = (EditText) myview.findViewById(R.id.editText12);
        etgl.setText(tgl);
        ejam.setText(jam);
        elks.setText(lks);
        ehal.setText(hal);
        fragmentManager = getActivity().getSupportFragmentManager();
        save = (Button) myview.findViewById(R.id.button6);
        save.setOnClickListener(this);
        return myview;
    }

    @Override
    public void onClick(View v) {
        tgl = etgl.getText().toString();
        jam = ejam.getText().toString();
        lks = elks.getText().toString();
        hal = ehal.getText().toString();
        if(tgl.isEmpty()||jam.isEmpty()||lks.isEmpty()||hal.isEmpty())
        {
            Toast.makeText(getActivity(), "Please Fill all the Field", Toast.LENGTH_LONG).show();
        }
        else {
            bgpc(id,tgl, jam, lks, hal);
        }
    }
    public void bgpc(final String id,String date, String time, String lokasi, String hal)
    {
        class Asyncpc extends AsyncTask<String, Void, String>
        {
            private Dialog loading;

            @Override
            protected void onPreExecute()
            {
                super.onPreExecute();
                loading = ProgressDialog.show(getActivity(), "Please wait", "Loading...");
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(getActivity(), s, Toast.LENGTH_LONG).show();
                ListJadwalTU A = new ListJadwalTU();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.Tholder, A);
                fragmentTransaction.commit();

            }

            @Override
            protected String doInBackground(String... params) {

                HashMap<String, String> data = new HashMap<>();
                data.put("id", params[0]);
                data.put("tgl", params[1]);
                data.put("jam", params[2]);
                data.put("lokasi", params[3]);
                data.put("hal",params[4]);

                Request ruc  = new Request();
                String result = ruc.Con(LOGIN_URL, data);

                return result;
            }

        }
        Asyncpc ulc = new Asyncpc();
        ulc.execute(id,date, time, lokasi, hal);
    }

}
