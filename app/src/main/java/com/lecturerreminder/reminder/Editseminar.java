package com.lecturerreminder.reminder;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class Editseminar extends Fragment implements View.OnClickListener{
    private AutoCompleteTextView Dopem1,Dopem2,Dpm1,Dpm2,TA;
    String date,time,hal,dp1,dp2,d1,d2,kor,id;
    EditText edate,etime,ehal;
    View myview;
    private ArrayAdapter<String> adapter;
    String myJSON;
    private FragmentTransaction fragmentTransaction;
    private FragmentManager fragmentManager;
    int x=0;
    Button save;
    JSONArray dosen = null;
    String[] ld = new String[100];
    private static final String TAG_RESULTS = "result";
    private static final String TAG_NAME = "name";
    private static final String JSON_URL = "http://mobilreminder.pe.hu/editdosen.php";
    private static final String LOGIN_URL = "http://mobilreminder.pe.hu/edit_seminar.php";

    public Editseminar() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        myview = inflater.inflate(R.layout.fragment_editseminar, container, false);
        getData(JSON_URL);
        id = getArguments().getString("id");
        date = getArguments().getString("tgl");
        time = getArguments().getString("jam");
        hal = getArguments().getString("hal");
        dp1 = getArguments().getString("dopem1");
        dp2 = getArguments().getString("dopem2");
        d1 = getArguments().getString("dp1");
        d2 = getArguments().getString("dp2");
        kor = getArguments().getString("kor");
        Dopem1 = (AutoCompleteTextView) myview.findViewById(R.id.Dpem1);
        Dopem2 = (AutoCompleteTextView) myview.findViewById(R.id.Dpem2);
        Dpm1 = (AutoCompleteTextView) myview.findViewById(R.id.Dpm1);
        Dpm2 = (AutoCompleteTextView) myview.findViewById(R.id.Dpm2);
        TA = (AutoCompleteTextView) myview.findViewById(R.id.TA);
        edate = (EditText) myview.findViewById(R.id.sedate);
        etime = (EditText) myview.findViewById(R.id.setime);
        ehal = (EditText) myview.findViewById(R.id.editText6);
        Dopem1.setText(dp1);
        Dopem2.setText(dp2);
        Dpm1.setText(d1);
        Dpm2.setText(d2);
        TA.setText(kor);
        edate.setText(date);
        etime.setText(time);
        ehal.setText(hal);
        fragmentManager = getActivity().getSupportFragmentManager();
        save = (Button) myview.findViewById(R.id.button2);
        save.setOnClickListener(this);
        return myview;
    }

    public void onClick(View v) {
        d1 = Dopem1.getText().toString();
        d2 = Dopem2.getText().toString();
        dp1 = Dpm1.getText().toString();
        dp2 = Dpm2.getText().toString();
        kor = TA.getText().toString();
        date = edate.getText().toString();
        time = etime.getText().toString();
        hal = ehal.getText().toString();
        if (d1.isEmpty() || d2.isEmpty() || dp1.isEmpty() || dp2.isEmpty() || kor.isEmpty() || date.isEmpty() || time.isEmpty() || hal.isEmpty()) {
            Toast.makeText(getActivity(), "Please Fill all the Field", Toast.LENGTH_LONG).show();
        } else {
            bgpc(id,d1,d2,dp1,dp2,kor,date,time,hal);
        }
    }
    public void bgpc(final String id, String d1, String d2, String dp1, String dp2,String kor, String date, String time, String hal)
    {
        class Asyncpc extends AsyncTask<String, Void, String>
        {
            private Dialog loading;

            @Override
            protected void onPreExecute()
            {
                super.onPreExecute();
                loading = ProgressDialog.show(getActivity(), "Please wait", "Loading...");
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(getActivity(), s, Toast.LENGTH_LONG).show();
                ListJadwalTU A = new ListJadwalTU();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.Tholder, A);
                fragmentTransaction.commit();

            }

            @Override
            protected String doInBackground(String... params) {

                HashMap<String, String> data = new HashMap<>();
                data.put("id", params[0]);
                data.put("dop1", params[1]);
                data.put("dop2", params[2]);
                data.put("dopm1", params[3]);
                data.put("dopm2",params[4]);
                data.put("kor",params[5]);
                data.put("tgl",params[6]);
                data.put("jam",params[7]);
                data.put("perihal",params[8]);

                Request ruc  = new Request();
                String result = ruc.Con(LOGIN_URL, data);

                return result;
            }

        }
        Asyncpc ulc = new Asyncpc();
        ulc.execute(id,d1,d2,dp1,dp2,kor,date,time,hal);
    }


    public void getData(String url) {
        class GetDataJSON extends AsyncTask<String, Void, String> {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... params) {
                String uri = params[0];
                String result;
                Request ruc = new Request();
                result = ruc.get(uri);
                return result;
            }

            @Override
            protected void onPostExecute(String result) {
                myJSON = result;
                showList();

            }
        }
        GetDataJSON g = new GetDataJSON();
        g.execute(url);
    }

    private void showList() {
        try {
            JSONObject jsonObj = new JSONObject(myJSON);
            dosen = jsonObj.getJSONArray(TAG_RESULTS);
            for(int i=0;i<dosen.length();i++){
                JSONObject c = dosen.getJSONObject(i);
                ld[i] = c.getString(TAG_NAME);
            }
        }catch (JSONException e) {
            e.printStackTrace();
        }
        List<String> dosen = new ArrayList<String>();

        while(x<100)
        {
            if(ld[x]==null)
            {}
            else
            {
                dosen.add(ld[x]);
            }

            x++;
        }

        adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, dosen);
        Dopem2.setAdapter(adapter);
        Dopem1.setAdapter(adapter);
        Dpm1.setAdapter(adapter);
        Dpm2.setAdapter(adapter);
        TA.setAdapter(adapter);

    }


}
