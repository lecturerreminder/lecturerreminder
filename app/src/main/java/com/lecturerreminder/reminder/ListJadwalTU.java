package com.lecturerreminder.reminder;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.astuetz.PagerSlidingTabStrip;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListJadwalTU extends Fragment {

    View myview;
    public ListJadwalTU() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        myview = inflater.inflate(R.layout.fragment_list_jadwal_tu, container, false);
        ViewPager viewPager = (ViewPager) myview.findViewById(R.id.viewpager);
        viewPager.setAdapter(new SampleFragmentPagerAdapter(getChildFragmentManager()));

        // Give the TabLayout the ViewPager
        PagerSlidingTabStrip tabLayout = (PagerSlidingTabStrip ) myview.findViewById(R.id.tabs);
        tabLayout.setViewPager(viewPager);
        return myview;
    }
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
